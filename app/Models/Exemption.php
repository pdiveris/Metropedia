<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Exemption
 * 
 * @property integer id
 * @property string word
 * @property string root
 *
 */
class Exemption extends Model
{
    protected $table = 'exemptions';

    public $timestamps = false;

    protected $fillable = [
        'word',
        'root'
    ];

    protected $guarded = [];

        
}