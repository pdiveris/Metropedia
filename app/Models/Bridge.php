<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bridge
 * 
 * @property integer id
 * @property string what
 * @property string cStart
 * @property string cEnd
 * @property string venue
 * @property string info
 * @property string blurb
 * @property string lat
 * @property string lon
 * @property string locus
 * @property string type
 * @property string duration
 * @property string age
 * @property string image
 * @property string url
 * @property string source
 * @property DateTime timestamped
 * @property string bookingUrl
 * @property string genres
 * @property string lineup
 *
 */
class Bridge extends Model
{
    protected $table = 'bridge';

    public $timestamps = false;

    protected $fillable = [
        'what',
        'cStart',
        'cEnd',
        'venue',
        'info',
        'blurb',
        'lat',
        'lon',
        'locus',
        'type',
        'duration',
        'age',
        'image',
        'url',
        'source',
        'timestamped',
        'bookingUrl',
        'genres',
        'lineup'
    ];

    protected $guarded = [];

        
}