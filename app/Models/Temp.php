<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Temp
 * 
 * @property integer id
 *
 */
class Temp extends Model
{
    protected $table = 'temp';

    public $timestamps = false;

    protected $fillable = [];

    protected $guarded = [];

        
}