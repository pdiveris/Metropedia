<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 * 
 * @property integer id
 * @property string what
 * @property string cStart
 * @property string cEnd
 * @property string venue
 * @property string info
 * @property string director
 * @property string blurb
 * @property string lat
 * @property string lon
 * @property string locus
 * @property string type
 * @property string duration
 * @property string age
 * @property string image
 * @property string url
 * @property string video
 * @property string source
 * @property DateTime timestamped
 * @property string bookingUrl
 * @property string genres
 * @property string lineup
 * @property string localimage
 * @property string spare
 *
 */
class Event extends Model
{
    protected $table = 'events';

    public $timestamps = false;

    protected $fillable = [
        'what',
        'cStart',
        'cEnd',
        'venue',
        'info',
        'director',
        'blurb',
        'lat',
        'lon',
        'locus',
        'type',
        'duration',
        'age',
        'image',
        'url',
        'video',
        'source',
        'timestamped',
        'bookingUrl',
        'genres',
        'lineup',
        'localimage',
        'spare'
    ];

    protected $guarded = [];

        
}