<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Configuration
 * 
 * @property integer id
 * @property string name
 * @property string type
 * @property string value
 * @property integer ownerId
 * @property string role
 * @property integer permissions
 * @property string paramgroup
 * @property string timecreated
 * @property string timestamped
 *
 */
class Configuration extends Model
{
    protected $table = 'configuration';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'type',
        'value',
        'ownerId',
        'role',
        'permissions',
        'paramgroup',
        'timecreated',
        'timestamped'
    ];

    protected $guarded = [];

        
}