<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Text
 * 
 * @property integer id
 * @property string title
 * @property integer bookid
 * @property string corpus
 * @property string source
 * @property string sanitized
 * @property DateTime datum
 * @property string comments
 *
 */
class Text extends Model
{
    protected $table = 'texts';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'bookid',
        'corpus',
        'source',
        'sanitized',
        'datum',
        'comments'
    ];

    protected $guarded = [];

        
}