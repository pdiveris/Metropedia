<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Venue
 * 
 * @property integer id
 * @property string name
 * @property string address1
 * @property string assress2
 * @property string city
 * @property string postcode
 * @property string county
 * @property string latitude
 * @property string longitude
 * @property string phone
 * @property string website
 * @property string body
 *
 */
class Venue extends Model
{
    protected $table = 'venues';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'address1',
        'assress2',
        'city',
        'postcode',
        'county',
        'latitude',
        'longitude',
        'phone',
        'website',
        'body'
    ];

    protected $guarded = [];

        
}