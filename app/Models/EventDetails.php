<?php

namespace App\Models;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EventDetails
 *
 * @property integer id
 * @property string what
 * @property string cStart
 * @property string cEnd
 * @property string venue
 * @property string info
 * @property string director
 * @property string blurb
 * @property string lat
 * @property string lon
 * @property string locus
 * @property integer type
 * @property string duration
 * @property string age
 * @property string image
 * @property string url
 * @property string source
 * @property DateTime timestamped
 * @property string bookingUrl
 * @property string genres
 * @property string lineup
 * @property string url_what
 * @property integer cnt
  *
 */
class EventDetails extends Model
{
    protected $table = 'event_details';

    public $timestamps = false;

    protected $fillable = [

    ];

    protected $guarded = [];

        
}