<?php

namespace App\Models;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VenueView
 *
 * @property integer id
 * @property string name
 * @property string address1
 * @property string address2
 * @property string city
 * @property string postcode
 * @property string county
 * @property string latitude
 * @property string longitude
 * @property string phone
 * @property string website
 * @property string url_name
 * @property string body
 *
 */
class VenueView extends Model
{
    protected $table = 'venue_view';

    public $timestamps = false;

    protected $fillable = [

    ];

    protected $guarded = [];

        
}